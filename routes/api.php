<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\StatusController;

Route::resources([
    "status" => StatusController::class,
]);