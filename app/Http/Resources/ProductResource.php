<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class ProductResource extends JsonResource
{
    public function toArray(Request $request): array
    {
        $count = $this->code;
        $array = [];
        foreach ($this->materials as $material) {
            $size = $count * $material->pivot->quantity;
            $residual = intval($size);
            foreach ($material->warehouses as $warehouse) {
                $session = $warehouse->materials->name . $warehouse->id;
                if (session($session) !== 0) {
                    $history = $residual;
                    $residual -= session($session);
                    if ($residual > 0) {
                        $newArray = [
                            "warehouse_id" => $warehouse->id,
                            "material_name" => $material->name,
                            "qty" => $history - $residual,
                            "price" => $warehouse->price,
                        ];
                        $array[] = $newArray;
                        // $newArray = [
                        //     "warehouse_id" => null,
                        //     "material_name" => $material->name,
                        //     "qty" => $residual,
                        //     "price" => null,
                        // ];
                        // $array[] = $newArray;
                        if ($history < session($session)) {
                            session([$session => session($session) - $history]);
                        } else if ($history < session($session)) {
                            session([$session => session($session) - $history]);
                        } else {
                            session([$session => 0]);
                        }
                    } else if ($residual == 0) {
                        $newArray = [
                            "warehouse_id" => $warehouse->id,
                            "material_name" => $material->name,
                            "qty" => $warehouse->remainder,
                            "price" => $warehouse->price,
                        ];
                        $array[] = $newArray;
                        session([$session => session($session) - $history]);
                        break;
                    } else {
                        $newArray = [
                            "warehouse_id" => $warehouse->id,
                            "material_name" => $material->name,
                            "qty" => abs($history),
                            "price" => $warehouse->price,
                        ];
                        $array[] = $newArray;
                        session([$session => session($session) - $history]);
                    }
                } else {
                    continue;
                }
            }
        }
        return [
            "product_name" => $this->name,
            "product_qty" => $this->code,
            "product_materials" => $array,
        ];
    }
}
