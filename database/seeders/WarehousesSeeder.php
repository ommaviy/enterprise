<?php

namespace Database\Seeders;

use App\Models\Warehouses;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class WarehousesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Warehouses::create([
            "material_id" => 1,
            "remainder" => 12,
            "price" => 1500,
        ]);
        Warehouses::create([
            "material_id" => 1,
            "remainder" => 200,
            "price" => 1600,
        ]);
        Warehouses::create([
            "material_id" => 2,
            "remainder" => 40,
            "price" => 500,
        ]);
        Warehouses::create([
            "material_id" => 2,
            "remainder" => 300,
            "price" => 550,
        ]);
        Warehouses::create([
            "material_id" => 3,
            "remainder" => 200,
            "price" => 300,
        ]);
        Warehouses::create([
            "material_id" => 4,
            "remainder" => 1000,
            "price" => 2000,
        ]);
    }
}
