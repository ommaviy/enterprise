<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Warehouses extends Model
{
    use HasFactory;
    protected $fillable = [
        "material_id", "remainder", "price",
    ];
    public function materials(): BelongsTo
    {
        return $this->belongsTo(Material::class, "material_id");
    }
}
