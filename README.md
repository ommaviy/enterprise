# Enterprise
**Download the project**
### Enter the terminal
```
composer install
```
**Rename the .env.example file to .env .**
<br/>
**enter a base name or create a base named "enterprise".**

### Enter the terminal
```
php artisan migrate

php artisan db:seed

php artisan serve
```

**Open Postman or a browser and send a get request to http://127.0.0.1:8000/api/status**