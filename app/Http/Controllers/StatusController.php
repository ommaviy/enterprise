<?php

namespace App\Http\Controllers;

use App\Http\Resources\ProductResource;
use App\Models\Product;
use App\Models\Warehouses;
use Illuminate\Http\Request;

class StatusController extends Controller
{
    public function index()
    {
        $products = Product::all();
        $warehouses = Warehouses::all();
        foreach($warehouses as $warehouse) {
            session([
                $warehouse->materials->name.$warehouse->id => $warehouse->remainder
            ]);
        }
        return $this->response(ProductResource::collection($products));
    }

    public function store(Request $request)
    {
        //
    }

    public function show(string $id)
    {
        //
    }

    public function update(Request $request, string $id)
    {
        //
    }

    public function destroy(string $id)
    {
        //
    }
}
