<?php

namespace Database\Seeders;

use App\Models\Product;
use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{
    public function run(): void
    {
        $product = Product::create(["name" => "Ko'ylak", "code" => 30]);
        $product->materials()->attach([
            1 => ['quantity' => 0.8],
            2 => ['quantity' => 10],
            3 => ['quantity' => 5],
        ]);
        $product = Product::create(["name" => "Shim", "code" => 20]);
        $product->materials()->attach([
            1 => ['quantity' => 1.4],
            2 => ['quantity' => 15],
            3 => ['quantity' => 30],
            4 => ['quantity' => 1],
        ]);
    }
}
